import 'package:coffee_ui_design/util/assets.dart';
import 'package:coffee_ui_design/util/colors.dart';
import 'package:coffee_ui_design/util/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  var screenWidth, screenHeight;

  Widget buildCategory(categoryname, isSelected) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: Text(
        categoryname,
        style: TextStyle(
            color: isSelected ? AppColors.orange : AppColors.greycolor,
            fontSize: 15,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget buildCard(image, category, subcategory, price, rating, context) {
    return Container(
        width: screenWidth * 0.4 + 10,
        height: screenHeight * 0.3,
        margin: EdgeInsets.only(top: 15.0,right: 15,bottom: 15),
        padding: EdgeInsets.all(10.0),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: AppColors.greylight,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        child: Stack(
          children: [
            Column(
              children: [
                Expanded(
                  flex: 2,
                  child: Container(
                    decoration: BoxDecoration(
                        color: AppColors.greylight,
                        shape: BoxShape.rectangle,
                        borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        image: DecorationImage(
                            fit: BoxFit.cover, image: AssetImage(image))),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Expanded(
                  flex: 1,
                  child: Column(),
                )
              ],
            )
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;
    return SafeArea(
        child: Scaffold(
      backgroundColor: AppColors.backgroundcolor,
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: AppColors.greylight,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(8.0)),
                        ),
                        child: Icon(
                          Icons.grid_view_rounded,
                          color: AppColors.greycolor,
                          size: 20,
                        ),
                      ),
                      Container(
                        height: 40,
                        width: 40,
                        decoration: BoxDecoration(
                          color: AppColors.greylight,
                          shape: BoxShape.rectangle,
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          image: DecorationImage(
                            image: AssetImage(Assets.profile),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20),
                  Text(
                    AppStrings.findbest,
                    style: TextStyle(
                        color: AppColors.white,
                        fontSize: 30,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    AppStrings.coffeeforyou,
                    style: TextStyle(
                        color: AppColors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.w600),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 30),
                    child: TextField(
                      style: TextStyle(color: AppColors.white),
                      decoration: InputDecoration(
                          prefixIcon: Icon(
                            Icons.search,
                            color: AppColors.greycolor,
                          ),
                          hintText: AppStrings.searchstr,
                          hintStyle: TextStyle(
                            color: Color(0xff52555a),
                          ),
                          fillColor: Color(0xff141921),
                          filled: true,
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(10.0),
                          )),
                    ),
                  ),
                  SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        buildCategory(AppStrings.title1, true),
                        buildCategory(AppStrings.title2, false),
                        buildCategory(AppStrings.title3, false),
                        buildCategory(AppStrings.title4, false),
                        buildCategory(AppStrings.title5, false),
                      ],
                    ),
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    physics: BouncingScrollPhysics(),
                    child: Row(
                      children: [

                        buildCard(
                            Assets.coffee1,
                            AppStrings.title1,
                            AppStrings.subtitle1,
                            AppStrings.price1,
                            "4.0",
                            context),
                        buildCard(
                            Assets.coffee2,
                            AppStrings.title2,
                            AppStrings.subtitle2,
                            AppStrings.price2,
                            "4.1",
                            context),
                        buildCard(
                            Assets.coffee3,
                            AppStrings.title3,
                            AppStrings.subtitle3,
                            AppStrings.price3,
                            "4.2",
                            context),
                        buildCard(
                            Assets.coffee4,
                            AppStrings.title4,
                            AppStrings.subtitle4,
                            AppStrings.price4,
                            "4.3",
                            context),
                        buildCard(
                            Assets.coffee5,
                            AppStrings.title5,
                            AppStrings.subtitle5,
                            AppStrings.price5,
                            "4.4",
                            context),
                        buildCard(
                            Assets.coffee6,
                            AppStrings.title6,
                            AppStrings.subtitle6,
                            AppStrings.price6,
                            "5.0",
                            context),
                        buildCard(
                            Assets.coffee7,
                            AppStrings.title7,
                            AppStrings.subtitle7,
                            AppStrings.price7,
                            "3.2",
                            context),
                        buildCard(
                            Assets.coffee8,
                            AppStrings.title8,
                            AppStrings.subtitle8,
                            AppStrings.price8,
                            "4.9",
                            context)
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
