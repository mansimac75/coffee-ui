import 'dart:ui';

import 'colors.dart';

class FontFamily{
 FontFamily._();

  static const robotoMedium = 'Roboto Medium';
  static const robotoBold = 'Roboto Bold';
  static const robotoregular = 'Roboto Regular';
  static const robotoLight = 'Roboto Light';

  static  TextStyle titleInputStyle = TextStyle(
      color: AppColors.white,
      fontFamily: robotoBold,
      fontSize: 15.0);
}