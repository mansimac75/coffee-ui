class AppStrings{
  static const String findbest="Find The Best";
  static const String coffeeforyou="coffee for you";
  static const String searchstr="Find Your Coffee...";

  static const String title1="Capuccino";
  static const String title2="Espresso";
  static const String title3="Latte";
  static const String title4="Cold Coffee";
  static const String title5="Hot Chocolate";
  static const String title6="Bournvita";
  static const String title7="Hot Coffee";
  static const String title8="Milk Shake";

  static const String subtitle1="With Hot Milk";
  static const String subtitle2="With Hot Milk";
  static const String subtitle3="With Hot Milk";
  static const String subtitle4="With Cold Milk";
  static const String subtitle5="With Hot Chocolate";
  static const String subtitle6="With Hot Milk";
  static const String subtitle7="With Hot Milk";
  static const String subtitle8="With Cold Milk";

  static const String price1="4.2";
  static const String price2="3.14";
  static const String price3="4.7";
  static const String price4="5.4";
  static const String price5="4.4";
  static const String price6="4.7";
  static const String price7="4.2";
  static const String price8="3.25";


}