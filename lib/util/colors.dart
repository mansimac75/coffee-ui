import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors{
  static const Color backgroundcolor=Colors.black;
  static const Color greycolor=Color(0xff4d4f52);
  static const Color greylight=Color(0xff141921);
  static const Color white=Color(0xffffffff);
  static const Color orange=Color(0xffff9800);
}